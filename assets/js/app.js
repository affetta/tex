$(function() {

  $('.slick-main').slick({
    arrows: false,
    dots: true,
    adaptiveHeight: true,
    fade: true
  })

  $('.slick-card').slick({
    adaptiveHeight: true,
    slidesToShow: 2,
    arrows: false,
    dots: true
  })

  $('header').on('click', '.menu', function() {
    $('nav').toggle('slide', { 'direction': 'left' }, 300)
    return false
  })

  $('nav').on('click', '.close', function() {
    $('nav').toggle('slide', { 'direction': 'left' }, 300)
    return false
  })

  $('[type="text"]').each(function() {
    $(this).on('blur', function() {
      if ($(this).val().length > 0) {
        $(this).closest('label').addClass('active')
      } else {
        $(this).closest('label').removeClass('active')
      }
    })
  })

  $('header').on('focus', "input", function() {
  	$('.search-modal').show()
  })

  $('header').on('blur', "input", function() {
  	$('.search-modal').hide()
  })

  $('nav').on('click', '.dd', function() {
    $(this).toggleClass('active').stop().next('ul').toggle();
    return false;
  })

})